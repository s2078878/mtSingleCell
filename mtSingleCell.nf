#!/usr/bin/env nextflow

log.info """\
         =============================================================================================
         M T S I N G L E C E L L :   M T D N A   S I N G L E - C E L L   V A R I A N T   C A L L I N G
         =============================================================================================
         parameters_file:   ${params.parameters}
         =============================================================================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)

    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.bam, row.index, row.barcodes)}
        .set{ parameters_ch }
    
    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

/*
 * Extract only the mitochondrial reads from the bam file with minimum mapping quality of 30 and no secondary alignments
 */
process chrm {
    tag "Extracting mitochondrial reads"

    input:
    set row, sampleID, path(bam), path(index), path(barcodes) from parameters_ch

    output:
    set sampleID, path("${sampleID}.chrM.bam") into pseudo_bulk_ch
    set sampleID, path("${sampleID}.chrM.bam"), path("${sampleID}.chrM.bam.bai"), path(barcodes) into cell_level_ch

    script:
    """
    samtools view -h -b -q 30 -F 2304 ${bam} MT > ${sampleID}.chrM.bam
    samtools index ${sampleID}.chrM.bam
    """
}

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, path(chrM_bam) from pseudo_bulk_ch

    output:
    set sampleID, path("${sampleID}.pileup") into mpileup_ch

    script:
    """
    samtools sort ${chrM_bam} | \
    samtools mpileup -d 0 -f $baseDir/data/MT.fa - > ${sampleID}.pileup
    """
}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan {
    tag "Running VarScan2"

    input:
    set sampleID, path(pileup) from mpileup_ch

    output:
    set sampleID, path("${sampleID}.pseudoBulk.txt") into pseudo_ch

    script:
    """
    varscan mpileup2snp ${pileup} \
        --strand-filter 1 --min-avg-qual 30 \
        --min-coverage 1 --min-reads2 1 --min-var-freq 0 > ${sampleID}.pseudoBulk.txt
    """
}

/*
 * Split bam file into cells
 */
process split {
    tag "Splitting bam into cells"

    input:
    set sampleID, path(bam), path(index), path(barcodes) from cell_level_ch

    output:
    set sampleID, path("cell*.bam"), path(barcodes) into cells_ch

    script:
    """
    cp $baseDir/scripts/split.py . 
    python split.py ${bam} ${barcodes}
    """
}

/*
 * Call all mitochondrial variants for each cell
 */
process call {
    tag "Calling variants for each cell"

    input:
    set sampleID, path(cell_bams), path(barcodes) from cells_ch

    output:
    set sampleID, path("cell*.txt"), path(barcodes) into calls_ch

    script:
    """
    ls *.bam | parallel 'samtools index {}'
    cp $baseDir/scripts/call.py $baseDir/data/MT* .
    python call.py ${cell_bams}
    """
}

/*
 * Merge cell-level variant calls for one sample into a single file
 */
process merge {
    tag "Merging individual cell variant calls"

    input:
    set sampleID, path(calls), path(barcodes) from calls_ch

    output:
    set sampleID, path("${sampleID}.singleCell.txt") into cell_merged_ch

    script:
    """
    cp $baseDir/scripts/merge.R .
    Rscript merge.R cells ${sampleID} ${barcodes} ${calls} 
    """
}

/*
 * Combine cell-level and pseudo-bulk channels ready for cell-level filtering
 */
pseudo_ch
    .mix(cell_merged_ch)
    .groupTuple(by:0) 
    .map { sampleID, files -> tuple( sampleID, files.sort{it.name} ) }
    .map { input -> tuple(input[0], input[1][0], input[1][1]) }
    .set { merged_ch }
    
/*
 * Filter the cell-level variants using the pseudo-bulk calls
 */
process filter {
    tag "Filtering cell-level variant calls"

    input:
    set sampleID, path(pseudoBulk), path(cellLevel) from merged_ch
    
    output:
    path("${sampleID}.txt") into cell_filtered_ch

    script:
    """
    cp $baseDir/scripts/filter.R . 
    Rscript filter.R ${sampleID} ${pseudoBulk} ${cellLevel} 
    """
}

/*
 * Merge all of the samples variant calls
 */
process combine {
    tag "Merging all cell-level variant calls"

    input:
    path(calls) from cell_filtered_ch.collect()

    output:
    path("cellLevelCalls.txt") into results_ch

    script:
    """
    cp $baseDir/scripts/merge.R . 
    Rscript merge.R ${calls}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All mitochondrial DNA variants called for single-cells.\n" : "\nOops .. something went wrong.\n" )
}