import pysam
import csv
import sys

bam = sys.argv[1]
barcodes = sys.argv[2]

cell_dict = {}
with open(barcodes) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    header = next(csv_reader)
    for row in csv_reader:
        cell_dict[row[1]] = row[0]

cells = set(x for x in cell_dict.values())

bam = pysam.AlignmentFile(bam, "rb")

outs_dict = {}
for cell in cells:
    out = pysam.AlignmentFile("cell" + cell + ".bam", "wb", template = bam)
    outs_dict[cell] = out

for read in bam:
    tags = read.tags
    CB_list = [ x for x in tags if x[0] == "CB"]
    if CB_list:
        cell_barcode = CB_list[0][1]
    else: 
        continue
    cell_id = cell_dict.get(cell_barcode)
    if cell_id:
        outs_dict[cell_id].write(read)

bam.close()
for fout in outs_dict.values():
    fout.close()