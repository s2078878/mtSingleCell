# mtSingleCell - Single-Cell RNA-Sequencing mtDNA Variant Calling

This Nextflow pipeline will call mitochondrial genome SNVs from shallow depth single-cell RNA-sequencing data. 

## Running the Pipeline

### 1. Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtSingleCell.git
cd mtSingleCell
```

### 2. Create Environment

To create and activate a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
conda activate mtSingleCell
```

### 3. Make Support Files

The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names:

| row | sampleID | bam                                  | index                                |
|:----|:---------|:-------------------------------------|:-------------------------------------|
| 1   | sample1  | path/to/sample1/normal/alignment.bam | path/to/sample1/normal/index.bam.bai |
| 2   | sample1  | path/to/sample1/tumour/alignment.bam | path/to/sample1/tumour/index.bam.bai |
| 3   | sample2  | path/to/sample2/normal/alignment.bam | path/to/sample2/normal/index.bam.bai |
| 4   | sample2  | path/to/sample2/tumour/alignment.bam | path/to/sample2/tumour/index.bam.bai |

Alignment and index files can be for the entire genome, there is no need to subset to only the mitochondrial DNA.

### 4. Run mtSingleCell

Everything should now be ready to run mtSingleCell. A typical command will look something similar to the one below, substituting in the path to your own parameter file. Take additional note of setting the `--chr` parameter determining how the mitochondrial genome is denoted in the alignment files. 

```shell
nextflow run mtSingleCell.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 3)
  --outdir path/to/out/directory/ \ # where should the results folder be created? default: run directory
  --chr (chrM / MT) # how is the mitochondrial genome denoted? default: chrM
```

### 5. Outputs

The pipeline output file will be created in a results directory. This file will include all variant calls for individual cells for each sample input. 

## Notes

Please be aware that this pipelines `nextflow.config` file has been configured to run on the University of Edinburgh's Eddie HPC Cluster. This may need to be reconfigured to run on other computing systems. 







